@extends('layouts.navbar')

@section('body')

<h2>Newly Added Items</h2>
@if(isset($items))
<div class="row">
@foreach($items as $item)
<div class="col-sm-3">
<div class="card" style="width: 18rem;">
  <div id="carouselExampleControls-{{$item->id}}"   class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-inner">
    @php
      $first = true;
    @endphp

    @foreach(explode(',',$item->images) as $image)
    <div class="carousel-item {{$first ? 'active' : ''}}">
    @php
      $first = false;
    @endphp
    <img style="height:200px;" src="{{asset('images/items/') .'/'. $image}}" class="d-block w-100" alt="{{$image}}">
    </div>
    @endforeach
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls-{{$item->id}}" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls-{{$item->id}}" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
  </div>
  <div class="card-body">
    <h5 class="card-title">{{$item->name}}</h5>
    <p class="card-text">{{$item->description}}</p>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">{{$item->price}} {{$item->currency}}</li>
  </ul>
</div>
</div>
@endforeach
</div>
@else
<p>No newly added items!</p>
@endif

<h2>Newly Added Menus</h2>
@if(isset($menus))
<div class="row">
@foreach($menus as $menu)
<div class="col-sm-3">
<div class="card" style="width: 18rem;">
  <div id="carouselExampleControls-{{$menu->id}}"   class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-menu active">
    <img style="height:200px;" src="{{asset('images/menus/') .'/'. $menu->image}}" class="d-block w-100" alt="{{$menu->image}}">
    </div>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls-{{$menu->id}}" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls-{{$menu->id}}" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
  </div>
  <div class="card-body">
    <h5 class="card-title">{{$menu->name}}</h5>
    <p class="card-text">{{$menu->description}}</p>
    <a href="/menus/{{$menu->id}}"><button class="btn btn-primary" type="button">Show Menu</button></a>
  </div>
</div>
</div>
@endforeach
</div>
@else
<p>No newly added menus!</p>
@endif

<h2>Newly Added Offers</h2>
@if(isset($offers))
<div class="row">
@foreach($offers as $offer)
<div class="col-sm-3">
<div class="card" style="width: 18rem;">
  <div id="carouselExampleControls-{{$offer->id}}"   class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-menu active">
    <img style="height:200px;" src="{{asset('images/offers/') .'/'. $offer->image}}" class="d-block w-100" alt="{{$offer->image}}">
    </div>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls-{{$offer->id}}" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls-{{$offer->id}}" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
  </div>
  <div class="card-body">
    <h5 class="card-title">{{$offer->name}}</h5>
    <p class="card-text">{{$offer->description}}</p>
    <p class="card-text">{{$offer->price}} {{$offer->currency}}</p>
    <a href="/offers/{{$offer->id}}"><button class="btn btn-primary" type="button">Show Offer</button></a>
  </div>
</div>
</div>
@endforeach
</div>
@else
<p>No newly added menus!</p>
@endif

@stop