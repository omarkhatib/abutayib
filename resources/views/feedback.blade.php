@extends('layouts.navbar')

@section('body')

@push('styles')
    <link rel="stylesheet" href="https://cdn.reflowhq.com/v1/toolkit.min.css">
    <link rel="stylesheet" href="https://unpkg.com/star-rating.js@4.1.5/dist/star-rating.css"/>
    <link rel="stylesheet" href="https://unpkg.com/nice-select2@2.0.0/dist/css/nice-select2.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
    <link href="{{ asset('assets/css/feedback.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script src="https://cdn.reflowhq.com/v1/toolkit.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
<script src="https://unpkg.com/nice-select2@2.0.0/dist/js/nice-select2.js"></script>
<script src="https://unpkg.com/star-rating.js@4.1.5/dist/star-rating.js"></script>
<script src="{{ asset('assets/js/feedback.js') }}"></script>
@endpush

@if(isset($success))
    <div id="alert" class="alert alert-{{$success ? 'success' : 'danger' }} alert-dismissible fade show" role="alert">
        {{$msg}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <script>
        let alert = document.getElementById("alert")
        setTimeout(
        function() {
            alert.remove();
        }, 5000);

    </script>
@endif

<form onsubmit="return submitData()" action="feedback" method="POST">
    @csrf
    <span style="color: red;">@error('email'){{$message}}<br>@enderror</span>
    <label for="email">Email address</label>
    <input name="email" type="email" class="form-control" id="email" placeholder="name@example.com" value="{{old('email')}}">
    <textarea name="feedback" value="{{old('feedback')}}" class="form-control" id="feedback" placeholder="optional, write your feedback here..." rows="3"></textarea>
    <span style="color: red;">@error('visit-star-rating'){{$message}}<br>@enderror</span>
    <fieldset style="margin-bottom: 10px;">
        <legend>visit Rating</legend>
        
        <div>
            <span class="visit-star-rating">
                <select name="visit-star-rating" id="v-star-rating">
                    <option value="">Select a rating</option>
                    <option value="5">5 Stars</option>
                    <option value="4">4 Stars</option>
                    <option value="3">3 Stars</option>
                    <option value="2">2 Stars</option>
                    <option value="1">1 Star</option>
                </select>
                <span class="v-star-rating--stars">
                    <span data-value="1"></span>
                    <span data-value="2"></span>
                    <span data-value="3"></span>
                    <span data-value="4"></span>
                    <span data-value="5"></span>
                </span>
            </span>
        </div>
    </fieldset>

    <script>
        let visitRating = new StarRating('#v-star-rating' , {
            classNames: {
                active: 'gl-active',
                base: 'gl-star-rating',
                selected: 'gl-selected',
            },
            clearable: true,
            maxStars: 5,
            prebuilt: false,
            stars: null,
            tooltip: '',
        }
        )
    </script>

    <fieldset style="margin-bottom: 10px;">
        <legend>Optional items Rating</legend>
        <div id="items_rating">   
            
        </div>
    </fieldset>

    <button class="btn btn-primary" type="button" onclick="addItem()" style="margin-right: 10px;">Add Item</button>
    <button class="btn btn-danger" type="button" data-bs-toggle="modal" data-bs-target="#confirmClear" style="margin-right: 10px;">Clear Items</button>

    <hr>

    <input class="btn btn-primary" type="submit">

    <!-- Modal -->
    <div class="modal fade" id="confirmClear" tabindex="-1" aria-labelledby="confirmClearLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="confirmClearLabel">Modal title</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            this action will clear all items rating
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" onclick="clearItems()" data-bs-dismiss="modal" class="btn btn-danger">confirm</button>
        </div>
        </div>
    </div>
    </div>
</form>
@stop

<script>
    function addItem() {
    validation = validateItemRating()
    if(validation[0] == false) {
        alert(validation[1])

        return
    }
    
    let items = document.getElementById("items_rating")

    
    html = `<div class="item-container">
        <select name="item-ids[]" class="items-list">
            <option value="0" selected disabled hidden>Select an item</option>`

    @foreach ($items as $item)
    html += '<option value="'+ {{$item->id}} +'">{{$item->name}}</option>'
    @endforeach
        
    html += `</select>
    <div class="stars">
    <span class="gl-star-rating">
        <select name="item-star-rating[]" class="star-rating">
            <option value="">Select a rating</option>
            <option value="5">5 Stars</option>
            <option value="4">4 Stars</option>
            <option value="3">3 Stars</option>
            <option value="2">2 Stars</option>
            <option value="1">1 Star</option>
        </select>
        <span class="gl-star-rating--stars">
            <span data-value="1"></span>
            <span data-value="2"></span>
            <span data-value="3"></span>
            <span data-value="4"></span>
            <span data-value="5"></span>
        </span>
    </span>
    </div>

    <input name="item-feedback[]" class="form-control" type="text" placeholder="optional, write item feedback">

    <button onclick="deleteItem(this)" class="btn btn-danger delete-item">X</button>

    </div>`

    items.insertAdjacentHTML('beforeend' , html)

    starsObj = new StarRating('.star-rating' , {
        classNames: {
            active: 'gl-active',
            base: 'gl-star-rating',
            selected: 'gl-selected',
        },
        clearable: true,
        maxStars: 5,
        prebuilt: false,
        stars: null,
        tooltip: '',
    }
    )

    let items_list = document.getElementsByClassName('items-list')

    NiceSelect.bind(items_list[items_list.length - 1], {searchable: true})
}
</script>