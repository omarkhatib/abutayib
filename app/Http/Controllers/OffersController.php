<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\File; 

class OffersController extends Controller
{
    function index() {
        $offers = \App\Models\Offer::all();

        return view('offers',compact('offers'));
    }
}