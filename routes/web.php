<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\CreateItemController;
use App\Http\Controllers\CreateMenuController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\MenusController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SessionsController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\CreateOfferController;
use App\Http\Controllers\OffersController;
use App\Http\Controllers\OfferController;
use App\Http\Controllers\InfoController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\UpdateItemController;
use App\Http\Controllers\UpdateMenuController;
use App\Http\Controllers\UpdateOfferController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::get('/menus', function () {
    return view('menus');
});

Route::get('/about', [AboutController::class, 'index']);


Route::get('/reservation', function () {
    return view('reservation');
});

Route::post('/reservation', [ReservationController::class, 'postData']);

Route::get('/offers', [OffersController::class, 'index']);
Route::get('/offers/{id}', [OfferController::class, 'index']);

Route::get('/feedback', [FeedbackController::class, 'index']);


Route::get('/feedback', [FeedbackController::class, 'index']);
Route::post('/feedback', [FeedbackController::class, 'postData']);

Route::get('/menus', [MenusController::class, 'index']);

Route::get('/menus/{id}', [MenuController::class, 'index']);

Route::get('login', [FeedbackController::class, 'index']);

// Admins only

Route::get('/admin/login', [LoginController::class, 'index'])->middleware("guest");
Route::post('/admin/login', [LoginController::class, 'postData'])->middleware("guest");

Route::get('/admin', function () {return view('admin');})->middleware('auth');

Route::get('/admin/create/item', function () {return view('create_item');})->middleware("auth");
Route::post('/admin/create/item', [CreateItemController::class, 'postData'])->middleware("auth");

Route::get('/admin/update/item', [UpdateItemController::class, 'index'])->middleware("auth");
Route::get('/admin/update/item/{id}', [UpdateItemController::class, 'updateView'])->middleware("auth");
Route::post('/admin/update/item/{id}', [UpdateItemController::class, 'postData'])->middleware("auth");

Route::get('/admin/create/menu', [CreateMenuController::class, 'index'])->middleware("auth");
Route::post('/admin/create/menu', [CreateMenuController::class, 'postData'])->middleware("auth");

Route::get('/admin/update/menu', [UpdateMenuController::class, 'index'])->middleware("auth");
Route::get('/admin/update/menu/{id}', [UpdateMenuController::class, 'updateView'])->middleware("auth");
Route::post('/admin/update/menu/{id}', [UpdateMenuController::class, 'postData'])->middleware("auth");

Route::get('/admin/create/offer', [CreateOfferController::class, 'index'])->middleware("auth");
Route::post('/admin/create/offer', [CreateOfferController::class, 'postData'])->middleware("auth");

Route::get('/admin/update/offer', [UpdateOfferController::class, 'index'])->middleware("auth");
Route::get('/admin/update/offer/{id}', [UpdateOfferController::class, 'updateView'])->middleware("auth");
Route::post('/admin/update/offer/{id}', [UpdateOfferController::class, 'postData'])->middleware("auth");

Route::get('/admin/update/info', [InfoController::class, 'index'])->middleware("auth");
Route::post('/admin/update/info', [InfoController::class, 'postData'])->middleware("auth");

Route::get('/admin/register', [RegisterController::class, 'index'])->middleware("auth");
Route::post('/admin/register', [RegisterController::class, 'postData'])->middleware("auth");

Route::get('/admin/reservations', [ReservationController::class, 'showReservations'])->middleware("auth");

Route::get('/admin/visitsfeedback', [FeedbackController::class, 'showVisitsFeedback'])->middleware("auth");
Route::get('/admin/itemsfeedback', [FeedbackController::class, 'showItemsFeedback'])->middleware("auth");

Route::post('/admin/logout', [SessionsController::class, 'destroy'])->middleware("auth");