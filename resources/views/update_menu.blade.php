@extends('layouts.admin_navbar')

@push('styles')
    <link rel="stylesheet" href="https://cdn.reflowhq.com/v1/toolkit.min.css">
    <link rel="stylesheet" href="https://unpkg.com/nice-select2@2.0.0/dist/css/nice-select2.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
    <link href="{{ asset('assets/css/create_menu.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
<script src="https://cdn.reflowhq.com/v1/toolkit.min.js"></script>
<script src="https://unpkg.com/nice-select2@2.0.0/dist/js/nice-select2.js"></script>
<script src="{{ asset('assets/js/create_menu.js') }}"></script>
@endpush

@section('body')
<h1>Update Menu</h1>
@if(isset($success))
    <div id="alert" class="alert alert-{{$success ? 'success' : 'danger' }} alert-dismissible fade show" role="alert">
        {{$msg}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <script>
        let alert = document.getElementById("alert")
        setTimeout(
        function() {
            alert.remove();
        }, 5000);

    </script>
@endif
<form onsubmit="return submitData()" action='/admin/update/menu/{{$menu->id}}' method='POST' enctype="multipart/form-data">
    @csrf
    <div class="mb-3">
    <span style="color: red;">@error('name'){{$message}}<br>@enderror</span>
    <label for="name" class="form-label">Name</label>
    <input value="{{$menu->name}}" type="text" class="form-control" name="name" id="name" placeholder="Menu name">
    </div>

    <div class="mb-3">
    <label for="description" class="form-label">description</label>
    <textarea value="{{$menu->description}}" class="form-control" placeholder="optional, menu description" name="description" id="description" rows="3"></textarea>
    </div>

    <fieldset style="margin-bottom: 10px;">
        <legend>Menu Items</legend>
        <div id="items">   
        @foreach ($menu_items as $menu_item)
            <div class="item-container">
                <select id="select-{{$menu_item->id}}" name="items[]" class="items-list">
                    <option value="0" disabled hidden>Select an item</option>`
                    <option selected value="{{$menu_item->id}}">{{$menu_item->name}}</option>
                    @foreach ($items as $item)
                        <option value="'+ {{$item->id}} +'">{{$item->name}}</option>
                    @endforeach
                </select>
                <button onclick="deleteItem(this)" class="btn btn-danger delete-item">X</button>
            </div>
            <script>NiceSelect.bind(document.getElementById('select-{{$menu_item->id}}'), {searchable: true})</script>
        @endforeach
        </div>
    </fieldset>

    <button class="btn btn-primary" type="button" onclick="addItem()" style="margin-right: 10px;">Add Item</button>
    <button class="btn btn-danger" type="button" data-bs-toggle="modal" data-bs-target="#confirmClear" style="margin-right: 10px;">Clear Items</button>

    <hr>
    <input class="btn btn-primary" type="submit">
</form>

<!-- Modal -->
<div class="modal fade" id="confirmClear" tabindex="-1" aria-labelledby="confirmClearLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="confirmClearLabel">Modal title</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            this action will clear all items rating
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" onclick="clearItems()" data-bs-dismiss="modal" class="btn btn-danger">confirm</button>
        </div>
        </div>
    </div>
</div>

<script>
    function addItem() {
        validation = validateItems()
        if(validation[0] == false) {
            alert(validation[1])

            return
        }
        
        let items = document.getElementById("items")
        
        html = `<div class="item-container">
            <select name="items[]" class="items-list">
                <option value="0" selected disabled hidden>Select an item</option>`

        @foreach ($items as $item)
        html += '<option value="{{$item->id}}">{{$item->name}}</option>'
        @endforeach
            
        html += `</select>
        <button onclick="deleteItem(this)" class="btn btn-danger delete-item">X</button>
        </div>`

        items.insertAdjacentHTML('beforeend' , html)

        let items_list = document.getElementsByClassName('items-list')

        NiceSelect.bind(items_list[items_list.length - 1], {searchable: true})
    }
</script>
@stop