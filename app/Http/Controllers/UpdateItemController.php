<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File; 
use Illuminate\Support\Str;

class UpdateItemController extends Controller
{
    function postData(Request $req) {
        $req->validate([
            'price' => 'required|numeric|gt:0',
            'name' => 'required',
            'currency' => 'required',
        ]); 
        $item = \App\Models\Item::find($req->id);
        $item->name = $req->name;
        $item->price = $req->price;
        $item->currency = $req->currency;
        $item->description = $req->description == null ? "" : $req->description;

        $item->save();
        return view('update_item', [ "success" => true , "msg" => "item updated sucessfully :)" ], compact('item'));
    }

    function updateView($id) {
        $item = \App\Models\Item::find($id);
        return view('update_item',compact('item'));
    }

    function index() {
        $items = DB::table('items')->latest('id')->get();

        return view('update_items',compact('items'));
    }
}