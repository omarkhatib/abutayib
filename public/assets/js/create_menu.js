function validateItems() {
    let items = document.getElementsByClassName('items-list')
    let j = 0

    let items_id = []

    for(let i = 0 ; i < items.length ; i++) {
        if(items[i].nodeName == 'SELECT') {
            if(items[i].value == 0)
                return [false,"Unselected fields: " + items[0][items[i].selectedIndex].text ]
            if(items_id.includes(items[i].value))
                return [false,"duplicated item: " + items[0][items[i].selectedIndex].text ]
            items_id.push(items[i].value)
            j++
        }
    }
    return [true,""]
}

function validate() {
    let res
    let err
    if(document.getElementById("name").value === '') {
        return [false , "Please give the menu a name"]
    }
    if(document.getElementById("image").value === '') {
        return [false , "Please give the menu an image"]
    }

    [res,err] = validateItems()
    if (res == false) {
        return [res,err]
    }
    return [true,""]
}

function submitData() {
    validation = validate()
    if(validation[0] == false) {
        alert(validation[1])
        return false
    }

    return true
}

function clearItems() {
    let items = document.getElementById("items")
    while (items.hasChildNodes()) {
        items.removeChild(items.lastChild)
    }
}

function deleteItem(obj) {
    parent = obj.parentElement
    if(parent != null) {
        parent.remove()
    }
}