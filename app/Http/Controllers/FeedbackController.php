<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\feedbackvisit; 
use App\Models\feedbackitem; 
use DB;

class FeedbackController extends Controller
{
    function index() {
        $items = \App\Models\Item::all();

        return view('feedback',compact('items'));
    }

    function postData(Request $req) {
        $req->validate([
            'email' => 'required|email:rfc,dns',
            'visit-star-rating' => 'required|numeric|gt:0|lte:5',
        ]);
        DB::beginTransaction();

        try {
            DB::insert('insert into feedbackvisits (email,feedback,rate) values (?, ? ,?)' , [
                strtolower($req->input('email')),
                is_null($req->input('feedback')) ? "" : $req->input('feedback'),
                $req->input('visit-star-rating')
            ]);
            if( $req->input('item-ids') !== null ) {
                for($i = 0 ; $i < count($req->input('item-ids')) ; $i++) {
                    DB::insert('insert into feedbackitems (email,item_id,feedback,rate) values (?, ?, ? ,?)' , [
                        $req->input('email'),
                        $req->input('item-ids')[$i],
                        is_null($req->input('item-feedback')[$i]) ? "" : $req->input('item-feedback')[$i],
                        $req->input('item-star-rating')[$i]
                    ]);
                }
            }

            DB::commit();
            return FeedbackController::index()->with([ "success" => true , "msg" => "feedback submitted sucessfully :)" ]);
        } catch (\Exception $e) {
            DB::rollback();
            //print_r($e->getMessage());
            return FeedbackController::index()->with(["success" => false , "msg" => "failed to submit feedback :(" ]); 
        }
    }

    function showVisitsFeedback() {
        $visits_feedback = DB::table('feedbackvisits')->latest('id')->get();
        return view('show_visits_feedback',compact('visits_feedback'));
    }

    function showItemsFeedback() {
        $items_feedback = DB::table('feedbackitems')->latest('feedbackitems.id')
        ->join('items','items.id','=','feedbackitems.item_id')
        ->get();
        return view('show_items_feedback',compact('items_feedback'));
    }
}