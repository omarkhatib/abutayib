@extends('layouts.admin_navbar')

@section('body')

<h1>Create Admin</h1>
@if(isset($success))
    <div id="alert" class="alert alert-{{$success ? 'success' : 'danger' }} alert-dismissible fade show" role="alert">
        {{$msg}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <script>
        let alert = document.getElementById("alert")
        setTimeout(
        function() {
            alert.remove();
        }, 5000);
    </script>
@endif
<form method="post" action="/admin/register">
    @csrf
    <div class="mb-3">
    <span style="color: red;">@error('username'){{$message}}<br>@enderror</span>
    <label for="formGroupExampleInput" class="form-label">Username</label>
    <input type="text" name="username" class="form-control" id="formGroupExampleInput" placeholder="username">
    </div>
    <div class="mb-3">
    <span style="color: red;">@error('password'){{$message}}<br>@enderror</span>
    <label for="formGroupExampleInput2" class="form-label">Password</label>
    <input type="password" name="password" class="form-control" id="formGroupExampleInput2" placeholder="enter password">
    </div>
    <div class="mb-3">
    <span style="color: red;">@error('cpassword'){{$message}}<br>@enderror</span>
    <label for="formGroupExampleInput3" class="form-label">Confirm Password</label>
    <input type="password" name="cpassword" class="form-control" id="formGroupExampleInput3" placeholder="repeat password">
    </div>

    <input class="btn btn-primary" type="submit" value="submit" />
</form>

@stop