<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File; 
use Illuminate\Support\Str;

class AboutController extends Controller
{
    function index() {
        $infos = DB::table('infos')->latest('id')->limit(1)->first();
        return view('about',compact('infos'));
    }
}