<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Config;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ReservationController extends Controller
{
    function postData(Request $req) {
        $req->validate([
            'reserved_by' => 'required',
            'guest_count' => 'required|numeric|gt:0',
            'reservation_start_date' => 'required|date|after:now|before:resevation_end_date|date_format:"m/d/Y, g:i A"',
            'reservation_end_date' => 'required|date|after:reservation_start_date|date_format:"m/d/Y, g:i A"'
        ]);

        $reservation_count = (int)$req->input('guest_count');
        // because tables are evenly distributed
        if($reservation_count % 2 == 0) {
            $reservation_count = $reservation_count + 1;
        }

        $max_reservation_count = DB::table('max_reservation')->latest('id')->limit(1)->first()->max_reservation;
        if($reservation_count > $max_reservation_count) {
            return view('reservation')->withErrors(['max_reached_error' => 'Sorry, we cant accept this count of guests']);
        }

        $start_date = Carbon::parse($req->input('reservation_start_date'));
        $end_date = Carbon::parse($req->input('reservation_end_date'));
        $result = DB::table('reservations')
        ->where('start_date', '<=', $end_date)
        ->where('end_date', '>=', $start_date)
        ->sum('guests_count');

        if($result+$reservation_count > $max_reservation_count) {
            return view('reservation')->withErrors(['max_reached_error' => 'Sorry, we cant accept this count of guests']);
        }

        $reservation = new \App\Models\Reservation;
        $reservation->start_date = $start_date;
        $reservation->end_date = $end_date;
        $reservation->guests_count = $reservation_count;
        $reservation->reserved_by = $req->input('reserved_by');
        $reservation->save();

        return view('reservation',["success" => true, "msg" => "reservation submitted sucessfully :)"  ]);
    }

    function showReservations() {
        $reservations = DB::table('reservations')->latest('id')->get();
        return view('show_reservations',compact('reservations'));
    }
}