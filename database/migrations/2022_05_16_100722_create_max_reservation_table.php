<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('max_reservation', function (Blueprint $table) {
            $table->id();
            $table->integer('max_reservation');
        });

        DB::table('max_reservation')->insert(
            array(
                'id' => 1,
                'max_reservation' => 50
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('max_reservation');
    }
};
