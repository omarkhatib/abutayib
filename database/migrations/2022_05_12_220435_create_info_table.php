<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infos', function (Blueprint $table) {
            $table->id();
            $table->string('address');
            $table->decimal('latitude');
            $table->decimal('longitude');
            $table->string('phones');
            $table->timestamps();
        });

        DB::table('infos')->insert(
            array(
                'address' => 'barja, chouf',
                'phones' => '76755278,03040506',
                'longitude' => 35.440070,
                'latitude' => 33.646420
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infos');
    }
};
