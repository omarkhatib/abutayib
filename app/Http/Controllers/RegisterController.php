<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use DB;

class RegisterController extends Controller
{
    function index() {
        return view('register');
    }

    function postData(Request $req) {
        $attributes = $req->validate([
            'username' => ['required','unique:App\Models\User'],
            'password' => ['required','min:8'],
            'cpassword' => ['required','min:8','same:password'],
        ]);

        try {
            $user = User::Create($attributes);
            return RegisterController::index()->with(["success" => true , "msg" => "User " . strtolower($attributes['username']) . ", Created Successfully" ]);
        } catch (Exception $e) {
            return RegisterController::index()->with(["success" => false , "msg" => "Failed to Created User" ]);
        }
    }
}