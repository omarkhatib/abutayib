## Setup
1. `git clone https://codeberg.org/omarkhatib/abutayib.git`
2. install [xampp](https://www.apachefriends.org/index.html) and run it then start mysql
3. install composer from `https://getcomposer.org/download/`
    1. if php is not in the path, composer will ask to locate it, you find it in `xampp folder`
All next steps are in the terminal
4. `cd abutayib`
5. `composer install`
6. create database in mysql with name `abutayib`
7. `cp .env.example .env` (or `copy` instead of `cp` if on windows)
8. `php artisan migrate:fresh`
9. `php artisan db:seed`
10. `php artisan key:generate`
11. `php artisan serve`

now go to `http://127.0.0.1:8000` in the browser and app should appear if no all steps above completed successfully.

admin panel : http://127.0.0.1:8000/admin\
user: admin\
password: admin1234