@extends('layouts.navbar')

@section('body')
    <div class="card-group row">
        @if(isset($offers) && count($offers) > 0)
            @foreach($offers as $offer)
            <div class="col-sm-4" style="margin-bottom: 10px;">
                <div class="card">
                    <img style="height:350px" class="card-img-top w-100 d-block" src="{{asset('images/offers')}}/{{$offer->image}}">
                    <div class="card-body">
                        <h4 class="card-title">{{$offer->name}}</h4>
                        <p class="card-text">{{$offer->description}}<br></p>
                        <a href="/offers/{{$offer->id}}"><button class="btn btn-primary" type="button">Show Offer</button></a>
                    </div>
                </div>
            </div>
            @endforeach
        @else
            <p> No offers created, yet...
        @endif

    </div>
@stop