@extends('layouts.admin_navbar')

@section('body')

@if(isset($items_feedback))
    <table class="table table-striped">
    <thead>
        <tr>
        <th scope="col">Email</th>
        <th scope="col">Item ID</th>
        <th scope="col">Item Name</th>
        <th scope="col">Feedback</th>
        <th scope="col">Rate</th>
        </tr>
    </thead>
    <tbody>
        @foreach($items_feedback as $feedback)
        <tr>
            <td>{{$feedback->email}}</td>
            <td>{{$feedback->item_id}}</td>
            <td>{{$feedback->name}}</td>
            <td>{{$feedback->feedback}}</td>
            <td>{{$feedback->rate}}</td>
        </tr>
        @endforeach
    </tbody>
    </table>
@else
    <p>No items feddbacks yet!</p>
@endif

@stop