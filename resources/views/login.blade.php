@extends('layouts.admin_navbar')

@section('body')

<h1>Create Admin</h1>
<form method="post" action="login">
    @csrf
    <div class="mb-3">
    <span style="color: red;">@error('username'){{$message}}<br>@enderror</span>
    <label for="formGroupExampleInput" class="form-label">Username</label>
    <input type="text" name="username" class="form-control" id="formGroupExampleInput" placeholder="username">
    </div>
    <div class="mb-3">
    <span style="color: red;">@error('password'){{$message}}<br>@enderror</span>
    <label for="formGroupExampleInput2" class="form-label">Password</label>
    <input type="password" name="password" class="form-control" id="formGroupExampleInput2" placeholder="enter password">
    </div>

    <input class="btn btn-primary" type="submit" value="submit" />
</form>

@stop