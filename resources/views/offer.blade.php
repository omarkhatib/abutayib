@extends('layouts.navbar')

@section('body')

@push('styles')
<link rel="stylesheet" href="https://cdn.reflowhq.com/v1/toolkit.min.css">
@endpush

@if(isset($offer_name))
<h1>{{$offer_name}}</h1>
@endif

<div data-reflow-type="product-list" data-reflow-perpage="10" data-reflow-order="date_desc" data-reflow-show="image,name,excerpt,price">
    <div class="reflow-product-list">
        @if(isset($items))
            @foreach($items as $item)
            <div class="ref-products">
                <div class="ref-product" style="margin-bottom:20px;">
                    <div id="carouselExampleControls-{{$item->id}}"   class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner" style="width:150px;height:150px;">
                        @php
                            $first = true;
                        @endphp
                        @foreach(explode(',',$item->images) as $image)
                        <div class="carousel-item {{$first ? 'active' : ''}}">
                        @php
                            $first = false;
                        @endphp
                        <img style="height:200px;" src="{{asset('images/items/') .'/'. $image}}" class="d-block w-100" alt="{{$image}}">
                        </div>
                        @endforeach
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls-{{$item->id}}" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls-{{$item->id}}" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                    </div>
                        
                    <div class="ref-product-data">
                        <div class="ref-product-info">
                            <h5 style="margin-left:10px;" class="ref-name">{{$item->name}}</h5>
                            <p class="ref-excerpt">{{$item->description}}</p>
                        </div>
                        <del><p class="ref-price">{{$item->price}} {{$item->currency}}</p></del>
                    </div>
                </div>
            </div>
            @endforeach
        @endif
    </div>
    <div>
        <p style="background-color: #00000000;padding: 10px;/* border-radius: 10px; */border: 1px dashed black;text-align: center;">price: {{$offer_price}} {{$offer_currency}}</p>
    </div>
</div>
@stop