<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File; 
use Illuminate\Support\Str;

class UpdateOfferController extends Controller
{
    function postData(Request $req) {
        $req->validate([
            'name' => 'required',
            'items' => 'required',
            'price' => 'required',
            'currency' => 'required'
        ]); 
        $offer = \App\Models\Offer::find($req->id);
        $offer->name = $req->name;
        $offer->items = implode("," , $req->input('items'));
        $offer->description = $req->description == null ? "" : $req->description;
        $offer->price = $req->price;
        $offer->currency = $req->currency;

        $offer->save();

        $items = DB::table('items')->latest('id')->get();

        $offer_items = [];
        foreach(explode(',',$offer->items) as $item_id) {
            array_push($offer_items , \App\Models\Item::find($item_id));
        }

        return view('update_offer', [ "success" => true , "msg" => "offer updated sucessfully :)" ], compact('offer','offer_items','items'));
    }

    function updateView($id) {
        $offer = \App\Models\Offer::find($id);
        $items = DB::table('items')->latest('id')->get();

        $offer_items = [];
        foreach(explode(',',$offer->items) as $item_id) {
            array_push($offer_items , \App\Models\Item::find($item_id));
        }
        return view('update_offer',compact('offer','offer_items','items'));
    }

    function index() {
        $offers = DB::table('offers')->latest('id')->get();

        return view('update_offers',compact('offers'));
    }
}