<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File; 
use Illuminate\Support\Str;

class CreateItemController extends Controller
{
    function postData(Request $req) {
        //dd($req->all());
        $req->validate([
            'price' => 'required|numeric|gt:0',
            'name' => 'required',
            'images' => 'required',
            'currency' => 'required',
        ]);        

        DB::beginTransaction();

        $images = [];

        try {
            if($req->hasfile('images')) {
                foreach($req->file('images') as $file)
                {
                    $filename = public_path().'/images/items/'.(string) uniqid() . "." .strtolower($file->getClientOriginalExtension());
                    array_push($images , basename($filename));
                    $file->move(public_path().'/images/items/', $filename);
                }
            }
            DB::insert('insert into items (name,images,price,currency,description,created_at,updated_at) values (?, ? , ? ,? , ?,now(),now())' , [
                Str::camel($req->input('name')),
                implode(',',$images),
                $req->input('price'),
                $req->input('currency'),
                is_null($req->input('description')) ? "" : $req->input('description')
            ]);
            DB::commit();
            
            return view('create_item' , [ "success" => true , "msg" => "feedback submitted sucessfully :)" ]);
        } catch (\Exception $e) {
            DB::rollback();
            // TODO(omarkhatib): this is leaking informations!!!!
            // need to handle common errors instead of returning it directly
            foreach($images as $image) {
                File::delete(public_path().'/images/items/'.$image);
            }
            return view('create_item' , [ "success" => false , "msg" => $e->getMessage() ]); 
        }
    }
}