<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File; 
use Illuminate\Support\Str;

class InfoController extends Controller
{
    function postData(Request $req) {
        $req->validate([
            'address' => 'required',
            'phones' => 'required',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
        ]);

        try {
            $data = \App\Models\Info::find(1);
            $data->address = $req->address;
            $data->phones = $req->phones;
            $data->latitude = $req->latitude;
            $data->longitude = $req->longitude;
            $data->save();
            return InfoController::index()->with([ "success" => true , "msg" => "informations submitted sucessfully :)" ]);
        } catch (\Exception $e) {
            return InfoController::index()->with(["success" => false , "msg" => "failed to submit feedback :(" ]); 
        }
        
    }

    function index() {
        $infos = DB::table('infos')->latest('id')->limit(1)->first();

        return view('update_info',compact('infos'));
    }
}