<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    function index() {
        $items = DB::table('items')->latest('id')->limit(3)->get();
        $menus = DB::table('menus')->latest('id')->limit(3)->get();
        $offers = DB::table('offers')->latest('id')->limit(3)->get();

        return view('home',compact('items','offers','menus'));
    }
}