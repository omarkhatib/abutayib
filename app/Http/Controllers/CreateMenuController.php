<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\File; 
use Illuminate\Support\Str;


class CreateMenuController extends Controller
{
    function index() {
        $items = \App\Models\Item::all();

        return view('create_menu',compact('items'));
    }

    function postData(Request $req) {
        $req->validate([
            'name' => 'required',
            'image' => 'required',
            'items' => 'required'
        ]);

        DB::beginTransaction();

        $filename = "";

        try {
            $file = $req->file('image');
            
            $path = public_path().'/images/menus/';
            $filename = public_path().'/images/items/'.(string) uniqid() . "." .strtolower($file->getClientOriginalExtension());
            $file->move($path, $filename);
            
            DB::insert('insert into menus (name,image,items,description,created_at,updated_at) values (?, ? , ? ,? , now() , now())' , [
                Str::camel($req->input('name')),
                basename($filename),
                implode("," , $req->input('items')),
                is_null($req->input('description')) ? "" : $req->input('description')
            ]);
            DB::commit();
            
            return CreateMenuController::index()->with([ "success" => true , "msg" => "feedback submitted sucessfully :)" ]);
        } catch (\Exception $e) {
            DB::rollback();
            // TODO(omarkhatib): this is leaking informations!!!!
            // need to handle common errors instead of returning it directly
            File::delete(public_path().'/images/menus/'.$filename);
            return CreateMenuController::index()->with([ "success" => false , "msg" => $e->getMessage() ]); 
        }
    }
}