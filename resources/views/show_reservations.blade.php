@extends('layouts.admin_navbar')

@section('body')

@if(isset($reservations))
    <table class="table table-striped">
    <thead>
        <tr>
        <th scope="col">#</th>
        <th scope="col">Reserved By</th>
        <th scope="col">Guests Count</th>
        <th scope="col">Start Date</th>
        <th scope="col">End Date</th>
        </tr>
    </thead>
    <tbody>
        @foreach($reservations as $reservation)
        <tr>
            <th scope="row">{{$reservation->id}}</th>
            <td>{{$reservation->reserved_by}}</td>
            <td>{{$reservation->guests_count}}</td>
            <td>{{$reservation->start_date}}</td>
            <td>{{$reservation->end_date}}</td>
        </tr>
        @endforeach
    </tbody>
    </table>
@else
    <p>No Reservations yet!</p>
@endif

@stop