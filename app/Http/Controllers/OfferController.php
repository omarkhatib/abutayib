<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\File; 

class OfferController extends Controller
{
    function index($id) {
        $offer = \App\Models\Offer::find($id);
        if(is_null($offer)) {
            abort(404);
        }
        $items_id = $offer['items'];
        $offer_name = $offer['name'];
        $offer_price = $offer['price'];
        $offer_currency = $offer['currency'];

        $items = [];

        foreach(explode(',',$items_id) as $item_id) {
            array_push($items , \App\Models\Item::find($item_id));
        }

        return view('offer',compact(['items','offer_name','offer_price','offer_currency']));
    }
}