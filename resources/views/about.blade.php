@extends('layouts.navbar')

@push('scripts')
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
@endpush

@section('body')
<h1>About</h1>

<h4>Address</h4>
<p>{{ $infos->address}}</p>

<h4>Phones</h4>
@foreach( explode(",",$infos->phones) as $phone )
    <p>{{ $phone }}</p>
@endforeach

<div id="map"></div>
<style>
    #map {
    height: 400px;
    width: 100%;
    }
</style>

<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAk-wJ2Jy0nIN9pqUTz3AG3KfB-X3Hi2z0&callback=initMap&v=weekly"
      defer
    ></script>

<script>
    function initMap() {
        const uluru = { lat: {{$infos->latitude}}, lng: {{$infos->longitude}} };
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 15,
            center: uluru,
        });
        const marker = new google.maps.Marker({
            position: uluru,
            map: map,
        });
        }

        window.initMap = initMap;
</script>

@stop