@extends('layouts.navbar')

@section('body')

@push('styles')
<link rel="stylesheet" href="https://cdn.reflowhq.com/v1/toolkit.min.css">
@endpush

<div data-reflow-type="product-list" data-reflow-perpage="10" data-reflow-order="date_desc" data-reflow-show="image,name,excerpt,price">
    <div class="reflow-product-list">
        @if(isset($offers))
            @foreach($offers as $offer)
            <div class="ref-products">
                <div class="ref-product" style="margin-bottom:20px;">
                    <div class="ref-product-data">
                        <div class="ref-product-info">
                        <img style="height:200px;width:200px;" src="{{asset('images/offers/') .'/'. $offer->image}}" class="d-block w-100" alt="{{$offer->image}}">                        

                            <h5 style="margin-left:10px;" class="ref-name">{{$offer->name}}</h5>
                            <p style="padding-left:10px" class="ref-excerpt">{{$offer->description}}</p>
                        </div>
                        
                        <button class="btn btn-warning" onclick="location.href='/admin/update/offer/{{$offer->id}}'">Update</button>
                    </div>
                </div>
            </div>
            @endforeach
        @endif
    </div>
</div>
@stop