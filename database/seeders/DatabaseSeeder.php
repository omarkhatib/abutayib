<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            \DB::unprepared("INSERT INTO `feedbackitems` VALUES (1,'ms_omar_100@hotmail.com',20,'ktir taybin',5,NULL,NULL),(2,'ms_omar_100@hotmail.com',14,'',4,NULL,NULL),(3,'ms_omar_100@hotmail.com',3,'',3,NULL,NULL),(4,'khatibo228@gmail.com',13,'ma khassa bel italian',1,NULL,NULL),(5,'khatibo228@gmail.com',4,'bad cheese quality used',2,NULL,NULL);");
            \DB::unprepared("INSERT INTO `feedbackvisits` VALUES (1,'ms_omar_100@hotmail.com','Great view, great prices, great service.',5,NULL,NULL),(2,'khatibo228@gmail.com','akel ma tayiib, w tele3le cha3ra bel akel.',2,NULL,NULL);");
            \DB::unprepared("INSERT INTO `items` VALUES (1,'7up','627e20fb96bdc.jpeg,627e20fb972af.jpeg',20000.00,'LBP','','2022-05-13 06:12:27','2022-05-13 06:12:27',0),(2,'applePie','627e21572f5e1.jpeg,627e21572f9e2.jpg',50000.00,'LBP','','2022-05-13 06:13:59','2022-05-13 06:13:59',0),(3,'ceaserSalad','627e218f7156e.jpeg,627e218f719a1.jpeg',15.00,'USD','for 2 people','2022-05-13 06:14:55','2022-05-13 06:14:55',0),(4,'cheeseballs','627e21c6c4288.jpg,627e21c6c4557.jpg,627e21c6c47bb.jpg',9.00,'EUR','15 pieces','2022-05-13 06:15:50','2022-05-13 06:15:50',0),(5,'chickenSub','627e21ea61852.jpeg,627e21ea61b2b.jpg',95000.00,'LBP','','2022-05-13 06:16:26','2022-05-13 06:16:26',0),(6,'classicHummus','627e2220958fa.jpeg,627e222095d46.jpeg,627e222095fd7.jpg',45000.00,'LBP','with oil and spicy','2022-05-13 06:17:20','2022-05-13 06:17:20',0),(7,'creamyChickenPasta','627e225b273ac.jpeg,627e225b2776f.jpg',70.00,'USD','','2022-05-13 06:18:19','2022-05-13 06:18:19',0),(8,'crepe','627e228976a42.jpeg,627e228976d33.jpeg,627e228977079.jpg',60000.00,'LBP','with strawberry','2022-05-13 06:19:05','2022-05-13 06:19:05',0),(9,'crispyChicken','627e22c300d85.jpeg,627e22c301241.jpeg,627e22c30157d.jpg',100000.00,'LBP','tortilla bread','2022-05-13 06:20:03','2022-05-13 06:20:03',0),(10,'fajita','627e22f18df4e.jpeg,627e22f18e298.jpeg,627e22f18e56c.jpg',10.00,'EUR','with avocado sauce','2022-05-13 06:20:49','2022-05-13 06:20:49',0),(11,'fish&ChipsPlatter','627e2330e8376.jpg,627e2330e86bb.jpg',30.00,'AED','for 2 people','2022-05-13 06:21:52','2022-05-13 06:21:52',0),(12,'garlicBread','627e23624c710.jpeg,627e23624cb45.jpeg,627e23624ceac.jpg',40000.00,'LBP','20 pieces','2022-05-13 06:22:42','2022-05-13 06:22:42',0),(13,'italianPizza','627e239459333.jpeg,627e239459725.jpg',80.00,'USD','','2022-05-13 06:23:32','2022-05-13 06:23:32',0),(14,'lightBurger','627e23bebbc72.jpeg,627e23bebbf58.jpeg,627e23bebc215.jpg',50.00,'EUR','special sauce','2022-05-13 06:24:14','2022-05-13 06:24:14',0),(15,'mangoSalad','627e23ef39772.jpeg,627e23ef39b92.jpg',90.00,'EUR','for 1 people only','2022-05-13 06:25:03','2022-05-13 06:25:03',0),(16,'miranda','627e240d65cbb.jpeg,627e240d660c0.jpeg',20000.00,'LBP','','2022-05-13 06:25:33','2022-05-13 06:25:33',0),(17,'nachos','627e24357b2c6.jpeg,627e24357b628.jpeg,627e24357b9e1.jpg',30.00,'AED','many sauces','2022-05-13 06:26:13','2022-05-13 06:26:13',0),(18,'pepperoni','627e245d05eb7.jpeg,627e245d061dc.jpeg,627e245d06480.jpg',80.00,'USD','','2022-05-13 06:26:53','2022-05-13 06:26:53',0),(19,'pepsi','627e2471358c4.jpeg,627e247135ce4.jpeg',20000.00,'LBP','','2022-05-13 06:27:13','2022-05-13 04:59:02',0),(20,'spicyBuffalo','627e24aae2d29.jpeg,627e24aae3126.jpg',40.00,'EUR','plate or sandwich','2022-05-13 06:28:10','2022-05-13 06:28:10',0);");
            \DB::unprepared("INSERT INTO `menus` VALUES (1,'appetizers','627e25e009c41.jpg','4,6,12','','2022-05-13 06:33:20','2022-05-13 06:33:20'),(2,'dessert','627e26d0b2b79.jpg','2,8','','2022-05-13 06:37:20','2022-05-13 06:37:20'),(3,'pizza','627e271b960be.jpg','18,13','','2022-05-13 06:38:35','2022-05-13 06:38:35'),(4,'salads','627e27e96dcaf.jpg','3,15','healthy menu','2022-05-13 06:42:01','2022-05-13 06:42:01'),(5,'sandwich','627e286a67a4f.jpg','10,9,5','','2022-05-13 06:44:10','2022-05-13 06:44:10'),(6,'softdrinks','627e28ac006ac.jpg','19,16,1','','2022-05-13 06:45:16','2022-05-13 06:45:16');");
            \DB::unprepared("INSERT INTO `offers` VALUES (1,'mayMeal','627e2c5e43ac8.jpg','3,17,9,19,8','you can switch pepsi to any softdrinks',300000.00,'LBP','2022-05-13 07:01:02','2022-05-13 07:01:02');");
            \DB::unprepared("INSERT INTO `reservations` VALUES (1,'2022-05-17 10:20:00','2022-05-18 10:20:00',49,'omar','2022-05-16 07:20:56','2022-05-16 07:20:56'),(2,'2022-05-16 16:00:00','2022-05-16 17:00:00',21,'omar khatib','2022-05-16 07:40:08','2022-05-16 07:40:08'),(3,'2022-05-16 16:00:00','2022-05-16 17:00:00',25,'captain majed','2022-05-16 07:42:30','2022-05-16 07:42:30');");
            
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
        }
    }
}
