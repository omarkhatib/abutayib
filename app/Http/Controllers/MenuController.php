<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\File; 

class MenuController extends Controller
{
    function index($id) {
        $menu = \App\Models\Menu::find($id);
        if(is_null($menu)) {
            abort(404);
        }
        $items_id = $menu['items'];
        $menu_name = $menu['name'];

        $items = [];

        foreach(explode(',',$items_id) as $item_id) {
            array_push($items , \App\Models\Item::find($item_id));
        }

        return view('menu',compact(['items','menu_name']));
    }
}