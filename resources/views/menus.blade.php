@extends('layouts.navbar')

@section('body')
    <div class="card-group row">
        @if(isset($menus) && count($menus) > 0)
            @foreach($menus as $menu)
            <div class="col-sm-4" style="margin-bottom: 10px;">
                <div class="card">
                    <img style="height:350px" class="card-img-top w-100 d-block" src="{{asset('images/menus')}}/{{$menu->image}}">
                    <div class="card-body">
                        <h4 class="card-title">{{$menu->name}}</h4>
                        <p class="card-text">{{$menu->description}}<br></p>
                        <a href="/menus/{{$menu->id}}"><button class="btn btn-primary" type="button">Show Menu</button></a>
                    </div>
                </div>
            </div>
            @endforeach
        @else
            <p> No menus created, yet...
        @endif

    </div>
@stop