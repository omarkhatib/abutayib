<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File; 
use Illuminate\Support\Str;

class UpdateMenuController extends Controller
{
    function postData(Request $req) {
        $req->validate([
            'name' => 'required',
            'items' => 'required',
        ]); 
        $menu = \App\Models\Menu::find($req->id);
        $menu->name = $req->name;
        $menu->items = implode("," , $req->input('items'));
        $menu->description = $req->description == null ? "" : $req->description;

        $menu->save();
        $items = DB::table('items')->latest('id')->get();

        $menu_items = [];
        foreach(explode(',',$menu->items) as $item_id) {
            array_push($menu_items , \App\Models\Item::find($item_id));
        }
        return view('update_menu', [ "success" => true , "msg" => "menu updated sucessfully :)" ], compact('menu','menu_items','items'));
    }

    function updateView($id) {
        $menu = \App\Models\Menu::find($id);
        $items = DB::table('items')->latest('id')->get();

        $menu_items = [];
        foreach(explode(',',$menu->items) as $item_id) {
            array_push($menu_items , \App\Models\Item::find($item_id));
        }
        return view('update_menu',compact('menu','menu_items','items'));
    }

    function index() {
        $menus = DB::table('menus')->latest('id')->get();

        return view('update_menus',compact('menus'));
    }
}