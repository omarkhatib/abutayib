let starsObj = new StarRating('.star-rating' , {
        classNames: {
            active: 'gl-active',
            base: 'gl-star-rating',
            selected: 'gl-selected',
        },
        clearable: true,
        maxStars: 5,
        prebuilt: false,
        stars: null,
        tooltip: '',
    }
)

function deleteItem(obj) {
    parent = obj.parentElement
    if(parent != null) {
        parent.remove()
    }
}

function clearItems() {
    let items = document.getElementById("items_rating")
    while (items.hasChildNodes()) {
        items.removeChild(items.lastChild)
    }
}

function validateItemRating() {
    let items = document.getElementsByClassName('items-list')
    let stars_list = document.getElementsByClassName('star-rating')
    let j = 0

    let items_id = []

    for(let i = 0 ; i < items.length ; i++) {
        if(items[i].nodeName == 'SELECT') {
            if(items[i].value == 0)
                return [false,"Unselected fields: " + items[0][items[i].selectedIndex].text ]
            if(items_id.includes(items[i].value))
                return [false,"duplicated item: " + items[0][items[i].selectedIndex].text ]
            if(stars_list[j].value == '') {
                return [false,"please, fill the stars"]
            }
            items_id.push(items[i].value)
            j++
        }
    }
    return [true,""]
}

function getItemsRating() {
    const map = new Map()

    if(validateItemRating()[0] == false) return map

    let items = document.getElementsByClassName('items-list')
    let stars_list = document.getElementsByClassName('star-rating')
    let j = 0

    let items_id = []

    for(let i = 0 ; i < items.length ; i++) {
        if(items[i].nodeName == 'SELECT') {
            map.set(items[i].value , stars_list[j].value)
            j++
        }
    }
    return map
}

function getVisitRating() {
    let el = document.getElementById("v-star-rating")
    if (el.value == '') {
        return 0
    }
    return parseInt(el.value)
}

function submitData() {
    validation = validate()
    if(validation[0] == false) {
        alert(validation[1])
        return false
    }

    //clearFields()
    return true
}

function clearFields() {
    let email = document.getElementById("email")
    email.value = ""
    let feedback = document.getElementById("feedback")
    feedback.value = ""
    
    document.getElementById("v-star-rating").value = ''
    visitRating.rebuild()

    clearItems()
}

function validate() {
    let feedback = document.getElementById("feedback")
    let email = document.getElementById("email")
    if (email) {
        if (email.value == "") {
            return [false , "email is empty"]
        } else if(!validateEmail(email.value)) {
            return [false , "email malformatted"]
        }
    }

    let visitRating = getVisitRating()
    if (visitRating <= 0 || visitRating > 5) {
        return [false , "you must rate the visit"]
    } 

    [res,err] = validateItemRating()
    if (res == false) {
        return [res,err]
    }
    return [true,""]
}

function validateEmail(email) {
    return String(email)
        .toLowerCase()
        .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        ) != null
}