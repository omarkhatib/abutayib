@extends('layouts.admin_navbar')

@push('styles')
    <link rel="stylesheet" href="https://cdn.reflowhq.com/v1/toolkit.min.css">
    <link href="{{ asset('assets/css/create_menu.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script src="https://cdn.reflowhq.com/v1/toolkit.min.js"></script>
<script src="{{ asset('assets/js/create_menu.js') }}"></script>
@endpush

@section('body')
<h1>Update informations</h1>
@if(isset($success))
    <div id="alert" class="alert alert-{{$success ? 'success' : 'danger' }} alert-dismissible fade show" role="alert">
        {{$msg}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <script>
        let alert = document.getElementById("alert")
        setTimeout(
        function() {
            alert.remove();
        }, 5000);

    </script>
@endif
<form onsubmit="return submitData()" action='/admin/update/info' method='POST' enctype="multipart/form-data">
    @csrf
    <div class="mb-3">
    <span style="color: red;">@error('address'){{$message}}<br>@enderror</span>
    <label for="address" class="form-label">Address</label>
    <input value="{{$infos->address}}" type="text" class="form-control" name="address" id="address">
    </div>

    <div class="mb-3">
    <span style="color: red;">@error('phones'){{$message}}<br>@enderror</span>
    <label for="phones" class="form-label">Phones</label>
    <input value="{{$infos->phones}}" type="text" class="form-control" name="phones" id="phones" placeholder="xxxxxx,xxxxxx,xxxxx">
    </div>

    <div class="mb-3">
    <span style="color: red;">@error('latitude'){{$message}}<br>@enderror</span>
    <label for="latitude" class="form-label">Latitude</label>
    <input value="{{$infos->latitude}}" step=".00000001" class="form-control" type="number" name="latitude" id="latitude">
    </div>

    <div class="mb-3">
    <span style="color: red;">@error('longitude'){{$message}}<br>@enderror</span>
    <label for="longitude" class="form-label">Longitude</label>
    <input value="{{$infos->longitude}}" step=".00000001" class="form-control" type="number" name="longitude" id="longitude">
    </div>

    <hr>
    <input class="btn btn-primary" type="submit">
</form>

@stop