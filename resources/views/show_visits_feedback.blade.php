@extends('layouts.admin_navbar')

@section('body')

@if(isset($visits_feedback))
    <table class="table table-striped">
    <thead>
        <tr>
        <th scope="col">#</th>
        <th scope="col">Email</th>
        <th scope="col">Feedback</th>
        <th scope="col">Rating</th>
        </tr>
    </thead>
    <tbody>
        @foreach($visits_feedback as $feedback)
        <tr>
            <th scope="row">{{$feedback->id}}</th>
            <td>{{$feedback->email}}</td>
            <td>{{$feedback->feedback}}</td>
            <td>{{$feedback->rate}}</td>
        </tr>
        @endforeach
    </tbody>
    </table>
@else
    <p>No visit feddbacks yet!</p>
@endif

@stop