<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    function index() {
        return view('login');
    }

    function postData(Request $req) {
        $atrributes = $req->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        if(auth()->attempt($atrributes)) {
            session()->regenerate();
            return redirect('admin');
        }

        return back()->withErrors(['username' => 'Your provided credentials could not be verified.']);
    }
}
