<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\File; 

class MenusController extends Controller
{
    function index() {
        $menus = \App\Models\Menu::all();

        return view('menus',compact('menus'));
    }
}