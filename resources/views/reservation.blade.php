@extends('layouts.navbar')

@section('body')
@push('styles')
<link href="https://cdn.jsdelivr.net/gh/Eonasdan/tempus-dominus@v6.0.0-alpha14/dist/css/tempus-dominus.css" rel="stylesheet" crossorigin="anonymous">
@endpush

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-eMNCOe7tC1doHpGoWe/6oMVemdAVTMs2xqW4mwXrXsW0L84Iytr2wi5v2QjrP/xp" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/gh/Eonasdan/tempus-dominus@v6.0.0-alpha14/dist/js/tempus-dominus.js" crossorigin="anonymous"></script>
@endpush
<h1>Reservation</h1>

<form action="reservation" method="POST">
    @csrf
	
@if(isset($success))
    <div id="alert" class="alert alert-{{$success ? 'success' : 'danger' }} alert-dismissible fade show" role="alert">
        {{$msg}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <script>
        let alert = document.getElementById("alert")
        setTimeout(
        function() {
            alert.remove();
        }, 5000);

    </script>
@endif

<span style="color: red;">@error('reserved_by'){{$message}}<br>@enderror</span>
<input style="margin-bottom:10px;" type="text" class="form-control" name="reserved_by" placeholder="reserver name" aria-label="reserved_by"/>

<span style="color: red;">@error('max_reached_error'){{$message}}<br>@enderror</span>
<span style="color: red;">@error('guest_count'){{$message}}<br>@enderror</span>
<input type="number" class="form-control" name="guest_count" placeholder="people number" aria-label="people_number">
<div class='row'>
    <span style="color: red;">@error('reservation_start_date'){{$message}}<br>@enderror</span>
    <span style="color: red;">@error('reservation_end_date'){{$message}}<br>@enderror</span>
	<div class='col-sm-6'>
	  <label for='linkedPickers1Input' class='form-label'>From</label>
	  <div
		class='input-group log-event'
		id='linkedPickers1'
		data-td-target-input='nearest'
		data-td-target-toggle='nearest'
	  >
		<input
		  id='linkedPickers1Input'
          name="reservation_start_date"
		  type='text'
		  class='form-control'
		  data-td-target='#linkedPickers1'
		/>
		<span
		  class='input-group-text'
		  data-td-target='#linkedPickers1'
		  data-td-toggle='datetimepicker'
		>
		   <span class='fas fa-calendar'></span>
		 </span>
	  </div>
	</div>
    
	<div class='col-sm-6'>
	  <label for='linkedPickers2Input' class='form-label'>To</label>
	  <div
		class='input-group log-event'
		id='linkedPickers2'
		data-td-target-input='nearest'
		data-td-target-toggle='nearest'
	  >
		<input
		  id='linkedPickers2Input'
          name="reservation_end_date"
		  type='text'
		  class='form-control'
		  data-td-target='#linkedPickers2'
		/>
		<span
		  class='input-group-text'
		  data-td-target='#linkedPickers2'
		  data-td-toggle='datetimepicker'
		>
		   <span class='fas fa-calendar'></span>
		 </span>
	  </div>
	</div>
    </div>

    <input style="margin-top:10px;" class="btn btn-primary" type="submit" />

</form>

<script type="text/javascript">
    const linkedPicker1Element = document.getElementById('linkedPickers1');
    const linked1 = new tempusDominus.TempusDominus(linkedPicker1Element);
    const linked2 = new tempusDominus.TempusDominus(document.getElementById('linkedPickers2'), {
    useCurrent: false
    });

	//using subscribe method
    const subscription = linked2.subscribe(tempusDominus.Namespace.events.change, (e) => {
    });

    //using event listeners
    linkedPicker1Element.addEventListener(tempusDominus.Namespace.events.change, (e) => {
    });

    // event listener can be unsubscribed to:
    // subscription.unsubscribe();
</script>
@stop