@extends('layouts.admin_navbar')

@section('body')

<h1>Admin page</h1>

<div class="row">
  <div class="actions_box d-grid gap-4 col-3 mx-auto">
    <button onclick="location.href='/admin/create/item'" class="btn btn-primary" type="button">Create Item</button>
    <button onclick="location.href='/admin/update/item'" class="btn btn-primary" type="button">Update Item</button>
  </div>

  <div class="actions_box d-grid gap-4 col-3 mx-auto">
    <button onclick="location.href='/admin/create/menu'" class="btn btn-primary" type="button">Create Menu</button>
    <button onclick="location.href='/admin/update/menu'" class="btn btn-primary" type="button">Update Menu</button>
  </div>

  <div class="actions_box d-grid gap-4 col-3 mx-auto">
    <button onclick="location.href='/admin/create/offer'" class="btn btn-primary" type="button">Create Offer</button>
    <button onclick="location.href='/admin/update/offer'" class="btn btn-primary" type="button">Update Offer</button>
  </div>

  <div class="actions_box d-grid gap-4 col-3 mx-auto">
    <button onclick="location.href='/admin/register'" class="btn btn-primary" type="button">Create Admin</button>
    <button onclick="location.href='/admin/reservations'" class="btn btn-primary" type="button">Reservations</button>
    <button onclick="location.href='/admin/visitsfeedback'" class="btn btn-primary" type="button">Visits Feedback</button>
    <button onclick="location.href='/admin/itemsfeedback'" class="btn btn-primary" type="button">Items Feedback</button>
    <button onclick="location.href='/admin/update/info'" class="btn btn-primary" type="button">Informations</button>
  </div>
</div>

<style>
    .actions_box {
      padding: 10px;
      border:1px dashed black;
      border-radius:5px;
    }
</style>

@stop